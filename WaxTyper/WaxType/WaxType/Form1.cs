﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaxType
{
    public partial class frmMain : Form
    {
        // ******* GLOBAL VARIABLES *******
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        // ****** SEC/MIN ******

        private int Secs = 1;
        private int Mins = 0;
        private int interval;

        public frmMain()
        {
            InitializeComponent();
        }

        // ******************** LOAD FORM ********************

        private void frmMain_Load(object sender, EventArgs e)
        {
            UpdateTimerLabel();
        }

        // ******************** DRAG FROM ********************

        private void dragMe(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void pnlUp_MouseDown(object sender, MouseEventArgs e)
        {
            dragMe(sender, e);
        }

        private void label2_MouseDown(object sender, MouseEventArgs e)
        {
            dragMe(sender, e);
        }

        // ******************** NORMAL EVENTS ********************

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        //Start typing
        private void btnStop_Click(object sender, EventArgs e)
        {
            tic.Enabled = false;
            tic.Stop();
            lblPercent.Text = "-OFF-";
        }

        //Stop typing
        private void btnStart_Click(object sender, EventArgs e)
        {
            tic.Enabled = true;
            tic.Start();
            lblPercent.Text = "-ON-";

        }

        //Show Write
        private void btnWrite_Click(object sender, EventArgs e)
        {
            pnlWrite.Visible = true;
        }

        //Show Timer
        private void btnTimer_Click(object sender, EventArgs e)
        {
            pnlWrite.Visible = false;
        }

        private void tbSec_Scroll(object sender, EventArgs e)
        {
            Secs = tbSec.Value;
            UpdateTimerLabel();
        }

        private void tbMin_Scroll(object sender, EventArgs e)
        {
            Mins = tbMin.Value;
            UpdateTimerLabel();
        }

        private void UpdateTimerLabel()
        {
            lblTimer.Text = Mins.ToString() + " mins and " + Secs.ToString() + " secs";
            interval = (Secs * 1000) + (Mins * 60000);
            tic.Interval = interval;
        }

        private void tic_Tick(object sender, EventArgs e)
        {
            foreach (char ch in rtbMain.Text)
                SendKeys.Send(ch.ToString());
            SendKeys.Send("~");
        }
    }
}
